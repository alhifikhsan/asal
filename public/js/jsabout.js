var cek = false;
$(window).scroll(function(){
    var scroll = $(window).scrollTop();

    const proyek = document.getElementById("jumProyek");
    var numProyek = document.getElementById("jumProyek").getAttribute("value");

    const karyawan = document.getElementById("jumKaryawan");
    var numKaryawan = document.getElementById("jumKaryawan").getAttribute("value");

    const peralatan = document.getElementById("jumPeralatan");
    var numPeralatan = document.getElementById("jumPeralatan").getAttribute("value");

    function animateValue(obj, start, end, duration){
        let startTimestamp = null;
        const step = (timestamp) => {
            if(!startTimestamp) startTimestamp = timestamp;
            const progress = Math.min((timestamp - startTimestamp) / duration, 1);
            obj.innerHTML = Math.floor(progress * (end - start) + start);
            if(progress<1){
                window.requestAnimationFrame(step);
            }
        };
        window.requestAnimationFrame(step)
    }

    if(cek == false){
        console.log(cek);
        if(scroll >= 2050){
            animateValue(proyek, 0, numProyek, 1000);
            animateValue(karyawan, 0, numKaryawan, 1000);
            animateValue(peralatan, 0, numPeralatan, 1000);
            cek = true;
        }
    }
});