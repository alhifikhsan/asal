$(window).scroll(function(){
    var scroll = $(window).scrollTop();

    // Merubah warna navbar
    if(scroll >= 100){
        $('#mainNav').addClass("bg-black");
    }else{
        $('#mainNav').removeClass('bg-black');
    }

});

$(window).resize(function(){
    var screen = $(window).width();

    if(screen <= 756){
        // Merubah ukuran text saat layar di perkecil bagian header
        $('.jumbotron .display-4').css('font-size', '37px');
        $('.jumbotron p').css('font-size', '15px');
        // Merubah ukuran text saat layar di perkecil bagian About Perusahaan
        $('#mainAbout h1').css('font-size', '30px');
        $('#mainAbout p').css('font-size', '15px');
    }else{
        // Merubah ukuran text saat layar di perkecil bagian header
        $('.jumbotron .display-4').css('font-size', '50px');
        $('.jumbotron p').css('font-size', '20px');
        // Merubah ukuran text saat layar di perkecil bagian About Perusahaan
        $('#mainAbout h1').css('font-size', '40px');
        $('#mainAbout p').css('font-size', '15px');
    }

    // Merubah ukuran gambar pada About Perusahaan
    if(screen <= 1191){
        $('#imageKontruk').css('width', '380px');
    }else{
        $('#imageKontruk').css('width', '550px');
    }
});
