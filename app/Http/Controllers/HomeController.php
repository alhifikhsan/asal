<?php

namespace App\Http\Controllers;

use App\Models\Alat;
use App\Models\FotoDepan;
use App\Models\Karyawan;
use App\Models\Profile;
use App\Models\Proyek;
use App\Models\Rekan;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $jumlahalat = Alat::get()->count();
        $jumlahkaryawan = Karyawan::get()->count();
        $jumlahproyek = Proyek::get()->count();
        $jumlahrekan = Rekan::get()->count();
        return view('home',compact('jumlahalat','jumlahkaryawan','jumlahproyek','jumlahrekan'));
    }
}
