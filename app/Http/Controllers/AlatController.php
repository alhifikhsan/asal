<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Alat;

class AlatController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $alats = Alat::orderBy('id','DESC')->paginate(5);
        // dd($alats);
        // return $alats;
        return view('alats.index',compact('alats'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('alats.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request -> validate([
            'nama_alat'=>'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            'jumlah'=>'required',
        ]);


        $alat = new Alat;

        $alat->nama_alat = $request->nama_alat;
        $alat->jumlah = $request->jumlah;
        // $alat->image = $path;

        $alat->image = go($request->file('image'),'images/alat')['up'];

        $alat->save();

        return redirect()->route('alats.index')
                        ->with('Sukses','Alat telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Alat $alat)
    {
        return view('alats.show',compact('alat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Alat $alat)
    {
        return view('alats.edit',compact('alat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request->all();
        $request->validate([
            'nama_alat'=>'required',
            'jumlah'=>'required',
        ]);

        $alat = Alat::find($id);

        if(isset($request->image)){
            $alat->image = go($request->image,'images/alat')['up'];
        }

        $alat->nama_alat = $request->nama_alat;
        $alat->jumlah = $request->jumlah;
        $alat->save();

        return redirect()->route('alats.index')
        ->with('Sukses','Alat telah diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Alat $alat)
    {
        $alat->delete();

        return redirect()->route('alats.index')
        ->with('Sukses','Alat telah dihapus');
    }
}
