<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Alat;
use App\Models\Karyawan;
use App\Models\Proyek;
use App\Models\Rekan;


class WebController extends Controller
{
    public function home()
    {
        // mau looping pake get
        // untuk ambil satu data pertama pake first
        $karyawan = Karyawan::get();
        $proyek = Proyek::get();
        $rekan = Rekan::get();
        $alat = Alat::get();
        return view('welcome',compact('proyek','karyawan','rekan','alat'));
    }
}
