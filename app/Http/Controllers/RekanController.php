<?php

namespace App\Http\Controllers;

use App\Models\Rekan;
use Illuminate\Http\Request;

class RekanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rekans = Rekan::orderBy('id','DESC')->paginate(5);
        // return $rekans;
        return view('rekans.index',compact('rekans'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rekans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            'alamat' => 'required',
        ]);



        $rekan = new Rekan;

        $rekan->nama = $request->nama;
        $rekan->alamat = $request->alamat;
        $rekan->image = go($request->file('image'),'images/rekan')['up'];

        $rekan->save();

        return redirect()->route('rekans.index')
        ->with('Sukses','Rekan telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Rekan $rekan)
    {
        return view('rekans.show',compact('rekan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Rekan $rekan)
    {
        return view('rekans.edit',compact('rekan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama'=>'required',
            'alamat'=>'required',
        ]);

        $rekan = Rekan::find($id);

        if(isset($request->image)){
            $rekan->image = go($request->image,'images/rekan')['up'];
        }

        $rekan->nama = $request->nama;
        $rekan->alamat = $request->alamat;
        $rekan->save();

        return redirect()->route('rekans.index')
        ->with('Sukses', 'Rekan telah diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rekan $rekan)
    {
        $rekan->delete();

        return redirect()->route('rekans.index')
        ->with('Sukses','Rekan telah dihapus');
    }
}
