<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Karyawan;

class KaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $karyawans = Karyawan::orderBy('id','DESC')->paginate(5);
        return view('karyawans.index',compact('karyawans'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('karyawans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            'alamat' => 'required',
            'keahlian' => 'required',
            'jabatan' => 'required',
        ]);



        $karyawan = new Karyawan;

        $karyawan->nama = $request->nama;
        $karyawan->alamat = $request->alamat;
        $karyawan->keahlian = $request->keahlian;
        $karyawan->jabatan = $request->jabatan;
        $karyawan->image = go($request->file('image'),'images/karyawan')['up'];

        $karyawan->save();

        return redirect()->route('karyawans.index')
        ->with('Sukses','Karyawan telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Karyawan $karyawan)
    {
        return view('karyawans.show',compact('karyawan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Karyawan $karyawan)
    {
        return view('karyawans.edit',compact('karyawan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'keahlian' => 'required',
            'jabatan' => 'required',
        ]);

        $karyawan = Karyawan::find($id);

        if(isset($request->image)){
            $karyawan->image = go($request->image,'images/karyawan')['up'];
        }

        $karyawan->nama = $request->nama;
        $karyawan->alamat = $request->alamat;
        $karyawan->keahlian = $request->keahlian;
        $karyawan->jabatan = $request->jabatan;
        $karyawan->save();


        return redirect()->route('karyawans.index')
        ->with('Sukses', 'Karyawan telah diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Karyawan $karyawan)
    {
        $karyawan->delete();

        return redirect()->route('karyawans.index')
        ->with('Sukses','Karyawan telah dihapus');
    }
}
