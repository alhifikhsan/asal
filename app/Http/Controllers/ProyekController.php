<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Proyek;

class ProyekController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proyeks = Proyek::orderBy('id','DESC')->paginate(5);
        return view('proyeks.index',compact('proyeks'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('proyeks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_proyek' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            'pemberi_kerja' => 'required',
            'tanggal_kontrak' => 'required',
            'deskripsi' => 'required',
            'tahun' => 'required',
        ]);

        $proyek = new Proyek;

        $proyek->nama_proyek = $request->nama_proyek;
        $proyek->pemberi_kerja = $request->pemberi_kerja;
        $proyek->tanggal_kontrak = $request->tanggal_kontrak;
        $proyek->deskripsi = $request->deskripsi;
        $proyek->tahun = $request->tahun;
        $proyek->image = go($request->file('image'),'images/proyek')['up'];

        $proyek->save();

        return redirect()->route('proyeks.index')
        ->with('Sukses','Proyek telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Proyek $proyek)
    {
        return view('proyeks.show',compact('proyek'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Proyek $proyek)
    {
        return view('proyeks.edit',compact('proyek'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_proyek' => 'required',
            'deskripsi' => 'required',
            'tanggal_kontrak' => 'required',
            'pemberi_kerja' => 'required',
            'tahun' => 'required',
        ]);

        $proyek = Proyek::find($id);

        if(isset($request->image)){
            $proyek->image = go($request->image,'images/proyek')['up'];
        }

        $proyek->nama_proyek = $request->nama_proyek;
        $proyek->deskripsi = $request->deskripsi;
        $proyek->tanggal_kontrak = $request->tanggal_kontrak;
        $proyek->pemberi_kerja = $request->pemberi_kerja;
        $proyek->tahun = $request->tahun;
        $proyek->save();

        return redirect()->route('proyeks.index')
        ->with('Sukses', 'Proyek telah diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Proyek $proyek)
    {
        $proyek->delete();

        return redirect()->route('proyeks.index')
        ->with('Sukses','Proyek telah dihapus');
    }
}
