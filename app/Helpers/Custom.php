<?php

function go($data,$loc,$id_user = null) {
    $path       = $loc;
    $namaBerkas = rand(000,999).'-'.$data->getClientOriginalName();
    if($id_user != null){
        $user   = \app\User::findOrFail($id_user);
        $path   = $user->username.'/'.$loc;
        \Storage::makeDirectory('public/'.$path);
    }
    $up         = $data->storeAs('public/'.$path.'/', $namaBerkas);
    $arr = ['up' => basename($up) , 'path' => $path];
    return $arr;
  }
?>
