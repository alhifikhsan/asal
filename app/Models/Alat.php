<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Alat extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama_alat','jumlah','image'
    ];

    protected $appends = ['avatar'];
    public function getAvatarAttribute(){
        return url('storage/images/alat/'.$this->image);
    }
}
