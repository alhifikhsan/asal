<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Proyek extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama_proyek','pemberi_kerja','deskripsi','tanggal_kontrak','image','tahun'
    ];

    protected $appends = ['text_limit','avatar'];
    public function getTextLimitAttribute(){
        return Str::limit($this->deskripsi,150);
    }
    public function getAvatarAttribute(){
        return url('storage/images/proyek/'.$this->image);
    }
}
