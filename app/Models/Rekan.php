<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Rekan extends Model
{
    use HasFactory;
    protected $fillable = [
        'nama','alamat','image'
    ];
    protected $appends = ['avatar'];
    public function getAvatarAttribute(){
        return url('storage/images/rekan/'.$this->image);
    }

}
