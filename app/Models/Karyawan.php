<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    use HasFactory;
    protected $fillable = [
        'nama','image','alamat','keahlian', 'jabatan'
    ];
    protected $appends = ['avatar'];
    public function getAvatarAttribute(){
        return url('storage/images/karyawan/'.$this->image);
    }
}
