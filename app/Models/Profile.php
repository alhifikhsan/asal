<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Profile extends Model
{
    use HasFactory;

    protected $fillable = [
        'judul','deskripsi','email','hp','fax','alamat'
    ];

    protected $appends = ['text_limit'];
    public function getTextLimitAttribute(){
        return Str::limit($this->deskripsi,150);
    }
}
