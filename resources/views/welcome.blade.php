@extends('frontend.template')
@section('content')



<!-- tentang kami -->
<br><br><br><br><br><br>
<br><br><br><br><br><br>
<br><br><br><br><br><br>
<br><br><br><br><br><br>
<section id="tentang-kami" class="section tentang-kami">
    <br><br>
    <div class="container">
        <div class="col-md-10 col-md-offset-1 text-center">
            <h3 style="font-size: 50px;"><b>PT. GRAHA KREASINDO UTAMA</b></h3><br>
            <p style="font-size: 25px">PT. Graha Kreasindo Utama adalah perseroan terbatas yang bergerak dalam bidang jasa konstruksi, dengan spesifikasi pada bidang layanan jasa arsitektural dan teknik sipil. Perusahaan ini berdiri berdasarkan Akte Notaris WAHYU WARSITO, SH.M.Kn Nomor : 04, Tanggal 13 April 2015.</p>
            <p style="font-size: 25px">Perusahaan ini sejak berdirinya sampai dengan saat ini selalu aktif dalam penanganan pekerjaaan di beberapa lingkungan Pemkab/Pemkot, Pemprop dan Dinas Perhubungan serta Dinas yang terkait di lingkungan Propinsi Jawa Timur khususnya dan diseluruh wilayah Indonesia pada umumnya.</p>
            <p style="font-size: 25px">PT. Graha Kreasindo Utama berlokasi di desa Watutelenan RT07/RW 08 Pulisen Boyolali, Jawa Tengah.Lingkup kegiatan yang dilakukan meliputi Bangunan Gedung dan Bangunan Sipil. Dalam melaksanakan pekerjaannya PT.Graha Kreasindo Utama berusaha bekerja sama dengan kontraktor-kontraktor lain untuk joint operation.</p>
        </div>
    </div>
    </section><!-- tentang kami -->



<section id="lingkup" class="lingkup section"><!-- Lingkup -->
    <h2 style="text-align: center; font-size:30px">Lingkup Kegiatan Yang Dilakukan</h2>
    <br><br>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-sm-6 feature text-center">
            <span><div class="zoom"><img src="https://img.icons8.com/ultraviolet/80/000000/building.png"/></div></span>
            <div class="feature-content">
                <h5>Kontruksi Bangunan</h5>
                <p>Jasa pelaksanaan untuk kontruksi bangunan hunian tunggal dan koppel, gudang dan industri, Komersial, Pendidikan, Kesehatan, Dll</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 feature text-center">
            <span><div class="zoom"><img src="https://img.icons8.com/ultraviolet/80/000000/dam.png"/></div></span>
            <div class="feature-content">
                <h5>Kontruksi Sumber Daya Air</h5>
                <p>Jasa pelaksana untuk konstruksi saluran air, pelabuhan, DAM, dan prasarana Sumber Daya Air lainnya</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 feature text-center">
            <span><div class="zoom"><img src="https://img.icons8.com/ultraviolet/80/000000/road.png"/></div></span>
            <div class="feature-content">
                <h5>Kontruksi Jalan</h5>
                <p>Jasa pelaksana untuk konstruksi jalan raya (kecuali jalan layang), jalan, rel kereta api dan landas pacu bandara</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 feature text-center">
            <span><div class="zoom"><img src="https://img.icons8.com/ultraviolet/80/000000/bridge.png"/></div></span>
            <div class="feature-content">
                <h5>Kontruksi Jembatan </h5>
                <p>Jasa pelaksana konstruksi pekerjaan jembatan, jalan layang, terowongan dan subways</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 feature text-center">
            <span><div class="zoom"><img src="https://img.icons8.com/ultraviolet/80/000000/stadium.png"/></div></span>
            <div class="feature-content">
                <h5>Kontruksi Arena Outdoor</h5>
                <p>Jasa pelaksana konstruksi bangunan stadion untuk olahraga outdoor</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 feature text-center">
            <span><div class="zoom"><img src="https://img.icons8.com/ultraviolet/80/000000/performance.png"/></div></span>
            <div class="feature-content">
                <h5>Kontruksi Arena Indoor</h5>
                <p>Jasa pelaksana konstruksi bangunan fasilitas olah raga indoor dan fasilitas rekreasi</p>
            </div>
        </div>
    </div>
</div>
</section><!-- Lingkup -->



<section id="pengalaman" class="pengalaman section no-padding">
    <h2 style="text-align: center; font-size:30px">Beberapa Pengalaman Kerja Kami</h2>
    <br>
<div class="container-fluid">
    <div class="row no-gutter">
        @foreach ($proyek as $p)
        <div class="col-lg-3 col-md-6 col-sm-6 work">
            <a href="{{ $p->avatar}}" class="work-box">
                <img src="{{ $p->avatar}}" alt="">
                <div class="overlay">
                    <div class="overlay-caption">
                        <h5>{{$p->nama_proyek}}</h5>
                        <p>{{ \Carbon\Carbon::parse($p->tahun)->format('j F Y') }}</p>
                    </div>
                </div><!-- overlay -->
            </a>
        </div>
        @endforeach
    </div>

</div>

</section><!-- works -->

<section id="tim" class="section tim">
    <h2 style="text-align: center; font-size:30px">Tim Kami</h2>
    <br>
<div class="container">
    <div class="row">
        @foreach ($karyawan as $k)
        <div class="col-md-3 col-sm-6">
            <div class="person">
                <img src="{{$k->avatar}}" alt="" class="img-responsive">
                <div class="person-content">
                    <h4>{{$k->nama}}</h4>
                    <h5 class="role">{{$k->jabatan}}</h5>
                    <p>{{ $k->keahlian}}</p>
                </div>
            </div><!-- person -->
            <br>
        </div>
        @endforeach
    </div>
</div>
</section><!-- teams -->






<section id="tim" class="section darikami no-padding">
<div class="container-fluid">
    <div class="row no-gutter">
        <div class="flexslider">
            <ul class="slides">
                <li>
                    <div class="col-md-6">
                        <div class="avatar">
                            <img src="images/web/s1.jpeg" alt="" class="img-responsive">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <blockquote>
                            <p>"Kreatif dan Inovatif Dalam Berkarya"
                            </p>
                        </blockquote>
                    </div>
                </li>
                <li>
                    <div class="col-md-6">
                        <div class="avatar">
                            <img src="images/web/s2.jpeg" alt="" class="img-responsive">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <blockquote>
                            <p>"Kebersamaan , Profesionalisme , dan Ketekunan untuk Menjadi yang Terbaik."
                            </p>
                        </blockquote>
                    </div>
                </li>
                <li>
                    <div class="col-md-6">
                        <div class="avatar">
                            <img src="images/web/s3.jpeg" alt="" class="img-responsive">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <blockquote>
                            <p>"PT.Graha Kreasindo Utama Hadir Untuk Bersama Membangun Negeri"
                            </p>
                        </blockquote>
                    </div>
                </li>
            </ul>
        </div><!-- flexslider -->
    </div>
</div>
</section><!-- Carousel -->

<!-- Alat baru -->
<section id="alat" class="alat section">
    <h2 style="text-align: center; font-size:30px">Daftar Inventaris Peralatan</h2>
    <br><br>
<div class="container">
    <div class="row">
        @foreach ($alat as $a)
        <div class="col-md-4 col-sm-2 feature text-center">
            <span><img src="{{$a->avatar}}" style/></span>
            <div class="feature-content">
                <h5>{{$a->nama_alat}}</h5>
                <p>{{$a->jumlah}}</p>
            </div>
        </div>
        @endforeach
    </div>
</div>
</section>
<!-- Alat baru -->

<!-- Rekanan -->
<section id="rekan" class="rekan section">
<h2 style="text-align: center; font-size:30px">Daftar Rekanan</h2><br><br>
<div class="container-xl">
	<div class="row">
		<div class="col-lg-8 mx-auto">
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
                    @foreach($rekan as $r => $rekans)
					<div class="carousel-item {{$r == 0 ? 'active' : '' }}">
						<div class="img-box"><img src="{{$rekans->avatar}}" alt=""></div><br>
                        <h3>{{$rekans->nama}}</h3>
					</div>
                    @endforeach
				</div>
				<!-- Carousel controls -->
				<a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
					<i class="fa fa-angle-left"></i>
				</a>
				<a class="carousel-control-next" href="#myCarousel" data-slide="next">
					<i class="fa fa-angle-right"></i>
				</a>
			</div>
		</div>
	</div>
</div>
</section>

<!-- Rekanan Tutup -->


@endsection
