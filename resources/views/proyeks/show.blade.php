@extends('proyeks.template')

@section('content')

<div class="col-xs-4">
    <div class="card bg-light">
        <center>
        <div class="card-header"><h1>Data Proyek</h1></div>
        <div class="card-body">
            <strong>Nama Proyek :</strong>
            <h3 class="card-title text-black text-danger">{{ $proyek->nama_proyek }}</h3>
            <strong>Pemberi Kerja :</strong>
            <h3 class="card-title text-black text-danger">{{ $proyek->pemberi_kerja }}</h3>
            <strong>No & Tanggal Kontrak :</strong>
            <h3 class="card-title text-black text-danger">{{ $proyek->tanggal_kontrak }}</h3>
            <strong>Tahun :</strong>
            <h3 class="card-title text-black text-danger">{{ $proyek->tahun }}</h3>
            <strong>Deskripsi Proyek :</strong>
            <h3 class="card-title text-black text-danger">{{ $proyek->deskripsi }}</h3>
            <strong>Foto Proyek :</strong><br>
            <img src="{{$proyek->avatar}}" height="200" width="200" alt="">
        </div>
    </div>
</center>
<a class="btn btn-secondary" href="{{ route('proyeks.index') }}"> Back</a>
</div>
@endsection
