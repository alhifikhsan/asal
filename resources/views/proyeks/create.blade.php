@extends('proyeks.template')

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<div class="row">
    <div class="col-xs-20">
        <div class="card">
            <div class="card-body">
<form action="{{ route('proyeks.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
     <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <span class="floating-label" for="nama_proyek">Nama Proyek</span>
                <input type="text" class="form-control" id="nama_proyek" name="nama_proyek" placeholder="Nama Proyek">
            </div>
        </div>
        <hr>
        <div class="col-md-12">
            <div class="form-group">
                <span class="floating-label" for="pemberi_kerja">Pemberi Kerja</span>
                <input type="text" name="pemberi_kerja" class="form-control" id="pemberi_kerja" placeholder="Pemberi Kerja">
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <span class="floating-label" for="tanggal_kontrak">No dan Tanggal Kontrak</span>
                <input type="text" name="tanggal_kontrak" class="form-control" id="tanggal_kontrak" placeholder="No dan Tanggal Kontrak">
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <span class="floating-label" for="tahun">Tahun</span>
                <input type="date" name="tahun" class="form-control" id="tahun" placeholder="Tahun">
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <span class="floating-label" for="deskripsi">Deskripsi Proyek</span>
                <input type="text" name="deskripsi" class="form-control" id="deskripsi" placeholder="Deskripsi Proyek">
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <span class="floating-label" for="image">Foto Proyek</span>
                <input type="file" name="image" class="form-control" id="image" placeholder="Foto Proyek">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-right">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>

</form>
@endsection
