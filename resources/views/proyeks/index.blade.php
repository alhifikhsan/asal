@extends('proyeks.template')

@section('content')



@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

        <div class="col-xs-4">
            <div class="card bg-light">
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th class="text-center">Nama Proyek</th>
                            <th class="text-center">Pemberi Kerja</th>
                            <th class="text-center">No & Tanggal Kontrak</th>
                            <th class="text-center">Tahun</th>
                            <th class="text-center">Deskripsi</th>
                            <th class="text-center">Foto</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    @foreach ($proyeks as $proyek)
                    <tbody>
                        <tr>
                            <td class="text-center">{{ ++$i }}</td>
                            <td class="text-center">{{ Str::limit($proyek->nama_proyek, 5) }}</td>
                            <td class="text-center">{{ $proyek->pemberi_kerja }}</td>
                            <td class="text-center">{{ Str::limit($proyek->tanggal_kontrak, 5) }}</td>
                            <td class="text-center">{{ $proyek->tahun}}</td>
                            <td class="text-center">{{ Str::limit($proyek->deskripsi, 5) }}</td>
                            <td class="text-center"><img src="{{$proyek->avatar}}" height="75" width="75" alt=""></td>
                            <td class="text-center">
                                <form action="{{ route('proyeks.destroy',$proyek->id) }}" method="POST">

                                    <a class="btn btn-info btn-sm" href="{{ route('proyeks.show',$proyek->id) }}">Show</a>

                                    <a class="btn btn-primary btn-sm" href="{{ route('proyeks.edit',$proyek->id) }}">Edit</a>

                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>

                                </form>
                            </td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="float-right">
            <a class="btn btn-success" href="{{ route('proyeks.create') }}">Tambah Proyek</a>
        </div>
        {{$proyeks->links('vendor.pagination.bootstrap-4')}}


@endsection
