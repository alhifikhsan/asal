@extends('backend.template')

@section('content')


<!DOCTYPE html>
<html>
<head>
    <title>PT GRAHA KREASINDO UTAMA</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    @yield('content')
</div>

</body>
</html>

@endsection
