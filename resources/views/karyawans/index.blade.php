@extends('karyawans.template')

@section('content')



@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

        <div class="col-xs-4">
            <div class="card bg-light">
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th class="text-center">Nama</th>
                            <th class="text-center">Alamat</th>
                            <th class="text-center">Jabatan</th>
                            <th class="text-center">Keahlian</th>
                            <th class="text-center">Foto</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    @foreach ($karyawans as $karyawan)
                    <tbody>
                        <tr>
                            <td class="text-center">{{ ++$i }}</td>
                            <td class="text-center">{{ $karyawan->nama }}</td>
                            <td class="text-center">{{ $karyawan->alamat }}</td>
                            <td class="text-center">{{ $karyawan->jabatan}}</td>
                            <td class="text-center">{{ Str::limit($karyawan->keahlian,15)}}</td>
                            <td class="text-center"><img src="{{$karyawan->avatar}}" height="75" width="75" alt=""></td>
                            <td class="text-center">
                                <form action="{{ route('karyawans.destroy',$karyawan->id) }}" method="POST">

                                    <a class="btn btn-info btn-sm" href="{{ route('karyawans.show',$karyawan->id) }}">Show</a>

                                    <a class="btn btn-primary btn-sm" href="{{ route('karyawans.edit',$karyawan->id) }}">Edit</a>

                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>

                                </form>
                            </td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="float-right">
            <a class="btn btn-success" href="{{ route('karyawans.create') }}">Tambah Karyawan</a>
        </div>
        {{ $karyawans->links('vendor.pagination.bootstrap-4')}}


@endsection
