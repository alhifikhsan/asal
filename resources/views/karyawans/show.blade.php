@extends('karyawans.template')

@section('content')

<div class="col-xs-4">
    <div class="card bg-light">
        <center>
        <div class="card-header"><h1>Data Karyawan</h1></div>
        <div class="card-body">
            <strong>Nama Karyawan :</strong>
            <h3 class="card-title text-black text-danger">{{ $karyawan->nama }}</h3>
            <strong>Alamat Karyawan :</strong>
            <h3 class="card-title text-black text-danger">{{ $karyawan->alamat }}</h3>
            <strong>Jabatan Karyawan :</strong>
            <h3 class="card-title text-black text-danger">{{ $karyawan->jabatan }}</h3>
            <strong>Keahlian Karyawan :</strong>
            <h3 class="card-title text-black text-danger">{{ $karyawan->keahlian }}</h3>
            <strong>Foto Karyawan :</strong><br>
            <img src="{{$karyawan->avatar}}" height="200" width="200" alt="">
        </div>
    </div>
</center>
<a class="btn btn-secondary" href="{{ route('karyawans.index') }}"> Back</a>
</div>
@endsection
