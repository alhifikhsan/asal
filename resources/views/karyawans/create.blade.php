@extends('karyawans.template')

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<div class="row">
    <div class="col-xs-20">
        <div class="card">
            <div class="card-body">
<form action="{{ route('karyawans.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
     <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <span class="floating-label" for="nama">Nama Karyawan</span>
                <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Karyawan">
            </div>
        </div>
        <hr>
        <div class="col-md-12">
            <div class="form-group">
                <span class="floating-label" for="alamat">Alamat Karyawan</span>
                <input type="text" name="alamat" class="form-control" id="alamat" placeholder="Alamat Karyawan">
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <span class="floating-label" for="jabatan">Jabatan Karyawan</span>
                <input type="text" name="jabatan" class="form-control" id="jabatan" placeholder="Jabatan Karyawan">
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <span class="floating-label" for="keahlian">Keahlian Karyawan</span>
                <input type="text" name="keahlian" class="form-control" id="keahlian" placeholder="Keahlian Karyawan">
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <span class="floating-label" for="image">Foto Karyawan</span>
                <input type="file" name="image" class="form-control" id="image" placeholder="Foto Karyawan">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-right">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>

</form>
@endsection
