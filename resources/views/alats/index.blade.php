@extends('alats.template')

@section('content')



@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

        <div class="col-xs-4">
            <div class="card bg-light">
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th class="text-center">Nama Alat</th>
                            <th class="text-center">Jumlah Alat</th>
                            <th class="text-center">Icon</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    @foreach ($alats as $alat)
                    <tbody>
                        <tr>
                            <td class="text-center">{{ ++$i }}</td>
                            <td class="text-center">{{ $alat->nama_alat }}</td>
                            <td class="text-center">{{ $alat->jumlah }}</td>
                            <td class="text-center"><img src="{{$alat->avatar}}" height="75" width="75" alt=""></td>
                            <td class="text-center">
                                <form action="{{ route('alats.destroy',$alat->id) }}" method="POST">

                                    <a class="btn btn-info btn-sm" href="{{ route('alats.show',$alat->id) }}">Show</a>

                                    <a class="btn btn-primary btn-sm" href="{{ route('alats.edit',$alat->id) }}">Edit</a>

                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>

                                </form>
                            </td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="float-right">
            <a class="btn btn-success" href="{{ route('alats.create') }}">Tambah Alat</a>
        </div>



{{$alats->links('vendor.pagination.bootstrap-4')}}
@endsection
