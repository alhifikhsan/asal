@extends('alats.template')

@section('content')
<div class="row mt-5 mb-5">
    <div class="col-lg-12 margin-tb">
        <div class="float-right">
            <a class="btn btn-secondary" href="{{ route('alats.index') }}"> Back</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
<form action="{{ route('alats.update',$alat->id) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
     <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <span class="floating-label" for="nama_alat">Nama Alat</span>
                <input type="text" class="form-control" id="nama_alat" value="{{ $alat->nama_alat }}" name="nama_alat" placeholder="Nama Alat">
            </div>
        </div>
        <hr>
        <div class="col-md-12">
            <div class="form-group">
                <span class="floating-label" for="jumlah">Jumlah Alat</span>
                <input type="text" name="jumlah" class="form-control" value="{{ $alat->jumlah }}" id="jumlah" placeholder="Jumlah Alat">
            </div>
        </div>
        <hr>
        <div class="col-md-12">
            <div class="form-group">
                <span class="floating-label" for="image">Icon</span>
                <input type="file" name="image" class="form-control" id="image" placeholder="Icon" >
            </div>
            <div class="form-group">
                <img src="{{$alat->avatar}}" height="200" width="200" alt="" name="image">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-right">
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </div>

</form>
@endsection
