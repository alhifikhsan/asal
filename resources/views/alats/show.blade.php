@extends('alats.template')

@section('content')

<div class="col-xs-4">
    <div class="card bg-light">
        <center>
        <div class="card-header"><h1>Data Alat</h1></div>
        <div class="card-body">
            <strong>Nama Alat :</strong>
            <h3 class="card-title text-black text-danger">{{ $alat->nama_alat }}</h3>
            <strong>Jumlah Alat :</strong>
            <h3 class="card-title text-black text-danger">{{ $alat->jumlah }}</h3>
            <strong>Icon :</strong><br>
            <img src="{{$alat->avatar}}" height="200" width="200" alt="">
        </div>
    </center>
    </div>
    <a class="btn btn-secondary" href="{{ route('alats.index') }}"> Back</a>
</div>
@endsection
