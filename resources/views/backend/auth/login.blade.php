<!DOCTYPE html>
<html lang="en">

<head>

	<title>PT GRAHA KREASINDO UTAMA</title>
	<!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 11]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="" />
	<meta name="keywords" content="">
	<meta name="author" content="Phoenixcoded" />
	<!-- Favicon icon -->
	<link rel="icon" href="/images/web/img_logo.png" type="image/x-icon">

	<!-- vendor css -->
	<link rel="stylesheet" href="/css/style.css">




</head>

<!-- [ auth-signin ] start -->
<div class="auth-wrapper">
	<div class="auth-content">
		<div class="card">
			<div class="row align-items-center text-center">
				<div class="col-md-12">
					<div class="card-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
						<h4 class="mb-3 f-w-400">Login</h4>
						<div class="form-group mb-3">
							<label class="floating-label" for="email">{{ __('E-Mail Address') }}</label>
							<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
						</div>
						<div class="form-group mb-4">
							<label class="floating-label" for="password">{{ __('Password') }}</label>
							<input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" required autocomplete="current-password">
                            @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
						</div>
						<button class="btn btn-block btn-primary mb-2">{{ __('Login') }}</button>

                        @if (Route::has('password.request'))
						<p class="mb-2 text-muted">{{ __('Forgot Your Password?') }}<a href="{{ route('password.request') }}" class="f-w-400">Reset</a></p>
                        @endif

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- [ auth-signin ] end -->

<!-- Required Js -->
<script src="/js/vendor-all.min.js"></script>
<script src="/js/plugins/bootstrap.min.js"></script>
<script src="/js/ripple.js"></script>
<script src="/js/pcoded.min.js"></script>



</body>

</html>
