<!DOCTYPE html>
<html lang="en">

<head>
    <title>PT GRAHA KREASINDO UTAMA</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="">
    <meta name="author" content="Phoenixcoded" />
    <!-- Favicon icon -->
    <link rel="icon" href="/images/web/img_logo.png" type="image/x-icon">

    <!-- vendor css -->
    <link rel="stylesheet" href="/css/style.css">



</head>
<body class="">
	<!-- [ Pre-loader ] start -->
	<div class="loader-bg">
		<div class="loader-track">
			<div class="loader-fill"></div>
		</div>
	</div>
	<!-- [ Pre-loader ] End -->
	<!-- [ navigation menu ] start -->
	<nav class="pcoded-navbar menu-light ">
		<div class="navbar-wrapper  ">
			<div class="navbar-content scroll-div " >

				<div class="">
					<div class="main-menu-header">
						<!-- ganti foto user -->
						<img class="img-radius" src="/images/user/profile.png" alt="User-Profile-Image">
						<div class="user-details">
							<!-- ganti jadi nama user -->
							<div id="more-details">{{ Auth::user()->name }}<i class="fa fa-caret-down"></i></div>
						</div>
					</div>
					<div class="collapse" id="nav-user-link">
						<ul class="list-unstyled">
							<li class="list-group-item">
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf
                                <i class="feather icon-log-out m-r-5" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                       this.closest('form').submit();">
                            </i>Log Out</form></li>

						</ul>
					</div>
				</div>

				<ul class="nav pcoded-inner-navbar ">
					<li class="nav-item pcoded-menu-caption">
					    <label>Navigation</label>
					</li>
					<li class="nav-item">
					    <a href="{{route('dashboard')}}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>
					</li>

					<li class="nav-item pcoded-menu-caption">
					    <label>Master Data</label>
					</li>
					<li class="nav-item">
					    <a href="{{route('alats.index')}}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-box"></i></span><span class="pcoded-mtext">Alat</span></a>
					</li>
                    <li class="nav-item">
					    <a href="{{route('karyawans.index')}}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-user"></i></span><span class="pcoded-mtext">Karyawan</span></a>
					</li>
					<li class="nav-item">
					    <a href="{{route('proyeks.index')}}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-briefcase"></i></span><span class="pcoded-mtext">Proyek</span></a>
					</li>
                    <li class="nav-item">
					    <a href="{{route('rekans.index')}}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-users"></i></span><span class="pcoded-mtext">Rekan</span></a>
					</li>
				</ul>


			</div>
		</div>
	</nav>
	<!-- [ navigation menu ] end -->
	<!-- [ Header ] start -->
	<header class="navbar pcoded-header navbar-expand-lg navbar-light header-blue">
				<div class="collapse navbar-collapse">
					<ul class="navbar-nav ml-auto">
						<li>
							<div class="m-header">
								<a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
								<a href="#!" class="mob-toggler">
								<i class="feather icon-more-vertical"></i>
								</a>
							</div>
						</li>
						<li>
							<div class="dropdown drp-user">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="feather icon-user"></i>
								</a>
								<div class="dropdown-menu dropdown-menu-right profile-notification">
									<div class="pro-head">
										<!-- ganti foto profile -->
										<img src="/images/user/profile.png" class="img-radius" alt="User-Profile-Image">
										<!-- ganti nama admin -->
										<span>{{ Auth::user()->name }}</span>
										<a href="{{ route('logout') }}" class="dud-logout" title="Logout">
                                            <form method="POST" action="{{ route('logout') }}">
                                                @csrf
											<i class="feather icon-log-out" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                            this.closest('form').submit();"></i></form>
										</a>
									</div>
									<ul class="pro-body">
										<li><a class="dropdown-item">
                                            <form method="POST" action="{{ route('logout') }}">
                                            @csrf
                                            <i class="feather icon-lock" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                   this.closest('form').submit();">
                                       </i>Log Out</form></a></li>
									</ul>
								</div>
							</div>
						</li>
					</ul>
				</div>


	</header>
	<!-- [ Header ] end -->



<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Dashboard</h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="feather icon-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->

              @yield('content')
        <!-- [ Main Content ] end -->
    </div>
</div>


    <!-- Required Js -->
    <script src="/js/vendor-all.min.js"></script>
    <script src="/js/plugins/bootstrap.min.js"></script>
    <script src="/js/ripple.js"></script>
    <script src="/js/pcoded.min.js"></script>

<!-- Apex Chart -->
<script src="/js/plugins/apexcharts.min.js"></script>


<!-- custom-chart js -->
<script src="/js/pages/dashboard-main.js"></script>
</body>

</html>
