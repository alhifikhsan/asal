@extends('rekans.template')

@section('content')

<div class="col-xs-4">
    <div class="card bg-light">
        <center>
        <div class="card-header"><h1>Data Rekanan</h1></div>
        <div class="card-body">
            <strong>Nama Rekan :</strong>
            <h3 class="card-title text-black text-danger">{{ $rekan->nama }}</h3>
            <strong>Alamat :</strong>
            <h3 class="card-title text-black text-danger">{{ $rekan->alamat }}</h3>
            <strong>Logo :</strong><br>
            <img src="{{$rekan->avatar}}" height="200" width="200" alt="">
        </div>
    </div>
</center>
<a class="btn btn-secondary" href="{{ route('rekans.index') }}"> Back</a>
</div>
@endsection
