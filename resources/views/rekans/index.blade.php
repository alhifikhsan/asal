@extends('rekans.template')

@section('content')



@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

        <div class="col-xs-4">
            <div class="card bg-light">
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th class="text-center">Nama Rekan</th>
                            <th class="text-center">Alamat</th>
                            <th class="text-center">Logo</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    @foreach ($rekans as $rekan)
                    <tbody>
                        <tr>
                            <td class="text-center">{{ ++$i }}</td>
                            <td class="text-center">{{ $rekan->nama }}</td>
                            <td class="text-center">{{ $rekan->alamat }}</td>
                            <td class="text-center"><img src="{{$rekan->avatar}}" height="75" width="75" alt=""></td>
                            <td class="text-center">
                                <form action="{{ route('rekans.destroy',$rekan->id) }}" method="POST">

                                    <a class="btn btn-info btn-sm" href="{{ route('rekans.show',$rekan->id) }}">Show</a>

                                    <a class="btn btn-primary btn-sm" href="{{ route('rekans.edit',$rekan->id) }}">Edit</a>

                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>

                                </form>
                            </td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="float-right">
            <a class="btn btn-success" href="{{ route('rekans.create') }}">Tambah Rekanan</a>
        </div>
        {{$rekans->links('vendor.pagination.bootstrap-4')}}


@endsection
