@extends('backend.template')
@section('content')


  <!-- page statustic card start -->
  <div class="row">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h4 class="text-c-yellow">{{$jumlahalat}}</h4>
                        <h6 class="text-muted m-b-0">Alat</h6>
                    </div>
                    <div class="col-4 text-right">
                        <i class="feather icon-box f-28"></i>
                    </div>
                </div>
            </div>
            <div class="card-footer bg-c-yellow">
                <div class="row align-items-center">
                    <div class="col-9">
                        <p class="text-white m-b-0" >Add</p>
                    </div>
                    <div class="col-3 text-right">
                        <a class="feather icon-plus text-white f-24" href="{{route('alats.create')}}"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h4 class="text-c-green">{{$jumlahkaryawan}}</h4>
                        <h6 class="text-muted m-b-0">Karyawan</h6>
                    </div>
                    <div class="col-4 text-right">
                        <i class="feather icon-user f-28"></i>
                    </div>
                </div>
            </div>
            <div class="card-footer bg-c-green">
                <div class="row align-items-center">
                    <div class="col-9">
                        <p class="text-white m-b-0">Add</p>
                    </div>
                    <div class="col-3 text-right">
                        <a class="feather icon-plus text-white f-24" href="{{route('karyawans.create')}}"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h4 class="text-c-blue">{{$jumlahproyek}}</h4>
                        <h6 class="text-muted m-b-0">Proyek</h6>
                    </div>
                    <div class="col-4 text-right">
                        <i class="feather icon-briefcase f-28"></i>
                    </div>
                </div>
            </div>
            <div class="card-footer bg-c-blue">
                <div class="row align-items-center">
                    <div class="col-9">
                        <p class="text-white m-b-0">Add</p>
                    </div>
                    <div class="col-3 text-right">
                        <a class="feather icon-plus text-white f-24" href="{{route('proyeks.create')}}""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h4 class="text-c-green">{{$jumlahrekan}}</h4>
                        <h6 class="text-muted m-b-0">Rekanan</h6>
                    </div>
                    <div class="col-4 text-right">
                        <i class="feather icon-users f-28"></i>
                    </div>
                </div>
            </div>
            <div class="card-footer bg-c-red">
                <div class="row align-items-center">
                    <div class="col-9">
                        <p class="text-white m-b-0">Add</p>
                    </div>
                    <div class="col-3 text-right">
                        <a class="feather icon-plus text-white f-24" href="{{route('rekans.create')}}"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- page statustic card end -->
</div>



</div>


@endsection
