<?php

use App\Http\Controllers\AlatController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\KaryawanController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ProyekController;
use App\Http\Controllers\RekanController;
use App\Http\Controllers\WebController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [WebController::class, 'home'])->name('home');

Auth::routes();

// Auth::routes(['register' => false]);

// Route::get('about', [WebController::class,'about'])->name('about');

// Route::get('contact', [WebController::class,'contact'])->name('contact');

// Route::get('proyek', [WebController::class,'proyek'])->name('proyek');

// Route::get('karyawan', [WebController::class,'karyawan'])->name('karyawan');

Route::prefix('admin')->group(function () {
    Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');
    Route::resource('karyawans',KaryawanController::class);
    Route::resource('alats', AlatController::class);
    Route::resource('proyeks', ProyekController::class);
    Route::resource('rekans', RekanController::class);
    Route::resource('profiles', ProfileController::class);
});
